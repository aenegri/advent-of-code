import os
from argparse import ArgumentParser

from adventofcode.defaults import DAYS, YEARS, get_latest_day, get_latest_year

parser = ArgumentParser()

parser.add_argument(
  "--year",
  choices=YEARS,
  default=get_latest_year(),
  type=int,
)

parser.add_argument(
  "--day",
  choices=DAYS,
  default=get_latest_day(),
  type=int,
)

args = parser.parse_args()

root = os.path.dirname(os.path.abspath(__file__))
target = os.path.join(root, "adventofcode", f"year_{args.year}", f"{args.day:02}.py")

f = open(target)
lines = f.read().split("\n")
f.close()

for i in range(len(lines)):
  lines[i] = ' ' * 4 + lines[i]
  
print("\n".join(lines))
  
