# advent-of-code

Solutions for advent of code
```
python3 aoc.py --year {year} --day {day} --part {part} --test {True|False} --submit {True|False}
```

Run the bootstrap script to generate a solution file and download input data
```
python3 bootstrap.py --year {year} --day {day}
```

## Setup

For reading and writng to adventofcode.com, add a `.env` file with you session
```
AOC_SESSION={}
```

To setup Python dependencies, create a virtual environment
```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```
