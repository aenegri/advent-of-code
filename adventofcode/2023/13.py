
import collections
import itertools
from adventofcode.registry import register

test_map = """
#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#
"""

def diff(a: str, b: str) -> int:
  return sum(1 for letter_a, letter_b in zip(a, b) if letter_a != letter_b)

def find_horizontal_reflection(pattern: list[str], *, smudge_count: int = 0) -> int | None:
  for start_location in range(1, len(pattern)):
    i, j = start_location - 1, start_location
    diff_count = 0
    
    while i > 0 and j < len(pattern) - 1 and diff_count <= smudge_count:
      diff_count += diff(pattern[i], pattern[j])
      i -= 1
      j += 1
    
    diff_count += diff(pattern[i], pattern[j])    
    if diff_count == smudge_count:
      return start_location
  
  return None
      

def find_vertical_reflection(pattern: list[str], *, smudge_count: int = 0) -> int | None:
  rotated: list[str] = [""] * len(pattern[0])
  
  for row in pattern:
    for j, cell in enumerate(row):
      rotated[j] += cell
  
  return find_horizontal_reflection(rotated, smudge_count=smudge_count)

@register(2023, 13, 1, test=(test_map, 405))
def part_one(lines: list[str]):
  summary = 0
  all = "\n".join(lines)
  for chunk in all.split("\n\n"):
    pattern = chunk.split("\n")
    
    if cols := find_vertical_reflection(pattern):
      summary += cols
    elif rows := find_horizontal_reflection(pattern):
      summary += rows * 100
  return summary

@register(2023, 13, 2, test=(test_map, 400))
def part_two(lines: list[str]):
  summary = 0
  all = "\n".join(lines)
  for chunk in all.split("\n\n"):
    pattern = chunk.split("\n")
    if cols := find_vertical_reflection(pattern, smudge_count=1):
      summary += cols
    elif rows := find_horizontal_reflection(pattern, smudge_count=1):
      summary += rows * 100
  return summary
