
import functools
import re
from adventofcode.registry import register

test_init = "rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7"

def hash(text: str) -> int:
  return functools.reduce(lambda x, y: (x + ord(y))* 17 % 256, text, 0)

@register(2023, 15, 1, test=(test_init, 1320))
def part_one(lines: list[str]):
  init_seq = ("".join(lines)).split(",")
  return sum(hash(step) for step in init_seq)

@register(2023, 15, 2, test=(test_init, 145))
def part_two(lines: list[str]):
  init_seq = ("".join(lines)).split(",")
  boxes: list[list[tuple[str, int]]] = [[] for _ in range(256)]
  
  for step in init_seq:
    m = re.match(r"(\w+)([=-])(\d+)?", step)
    label, op = m.group(1), m.group(2)
    box = boxes[hash(label)]
    
    if op == "-":
      for i, (other_label, _) in enumerate(box):
        if label == other_label:
          box.pop(i)
          break
    elif op == "=":
      replaced = False
      focal_length = int(m.group(3))
      for i, (other_label, _) in enumerate(box):
        if label == other_label:
          box[i] = (label, focal_length)
          replaced = True
          break
      if not replaced:
        box.append((label, focal_length))
  
  return sum(
    (i + 1) * (j + 1) * focal_length
    for i, box in enumerate(boxes)
    for j, (_, focal_length) in enumerate(box)
  )
      
      