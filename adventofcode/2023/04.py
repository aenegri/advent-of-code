
from dataclasses import dataclass
import math
import re
from typing import Optional
from adventofcode.parse import ints
from adventofcode.registry import register

test_scratchcards = """
Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
"""

@dataclass
class ScratchCard:
  id: int
  winning_nums: set[int]
  guess_nums: set[int]
  copies: Optional[int] = 1
  
  @classmethod
  def from_str(cls, s: str):
    id_match = re.match(r"Card\s+(\d+)", s)
    id = int(id_match.group(1))
    parts = s.split(":")[1].split("|")
    winning_nums, guess_nums = set(ints(parts[0])), set(ints(parts[1]))
    return cls(id, winning_nums, guess_nums)
  
  def score(self) -> int:    
    return int(math.pow(2, self.match_count() - 1))

  def match_count(self) -> int:
    return len(self.guess_nums.intersection(self.winning_nums))


@register(2023, 4, 1, test=(test_scratchcards, 13))
def part_one(lines: list[str]):
  cards = [ScratchCard.from_str(l) for l in lines]
  total_points = sum((card.score() for card in cards))
  return total_points


@register(2023, 4, 2, test=(test_scratchcards, 30))
def part_two(lines: list[str]):
  cards = [ScratchCard.from_str(l) for l in lines]
  
  for card in cards:
    end = min(card.id + card.match_count(), len(cards))
    for i in range(card.id, end):
      cards[i].copies += card.copies
  
  total_cards = sum((card.copies for card in cards))
  return total_cards
