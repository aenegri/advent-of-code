
import collections
from dataclasses import dataclass
from enum import IntEnum, auto
from functools import cache
from typing import Callable

from adventofcode.registry import register

test_game = """
32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483
"""

class HandType(IntEnum):
  HIGH_CARD = auto()
  PAIR = auto()
  TWO_PAIR = auto()
  TRIPS = auto()
  FULL_HOUSE = auto()
  QUAD = auto()
  QUINT = auto()


@cache
def evaluate_hand_type(cards: str) -> HandType:
  card_counts = collections.defaultdict(lambda: 0)
  
  for card in cards:
    card_counts[card] += 1
    
  if len(card_counts) == 1:
    return HandType.QUINT
  elif len(card_counts) == 2:
    cnts = card_counts.values()
    if 1 in cnts or 4 in cnts:
      return HandType.QUAD
    return HandType.FULL_HOUSE
  elif len(card_counts) == 3:
    cnts = card_counts.values()
    if 3 in cnts:
      return HandType.TRIPS
    return HandType.TWO_PAIR
  elif len(card_counts) == 4:
    return HandType.PAIR
  return HandType.HIGH_CARD


@cache
def evaluate_hand_type_with_joker(cards: str) -> HandType:
  card_counts = collections.defaultdict(lambda: 0)
  
  for card in cards:
    card_counts[card] += 1
    
  if len(card_counts) == 1:
    return HandType.QUINT
  elif len(card_counts) == 2:
    if card_counts["J"] > 0:
      return HandType.QUINT
    cnts = card_counts.values()
    if 1 in cnts or 4 in cnts:
      return HandType.QUAD
    return HandType.FULL_HOUSE
  elif len(card_counts) == 3:
    cnts = card_counts.values()
    if card_counts["J"] in (2, 3) or (card_counts["J"] == 1 and 3 in cnts):
      return HandType.QUAD
    elif card_counts["J"] == 1:
      return HandType.FULL_HOUSE
    if 3 in cnts:
      return HandType.TRIPS
    return HandType.TWO_PAIR
  elif len(card_counts) == 4:
    if card_counts["J"] > 0:
      return HandType.TRIPS
    return HandType.PAIR
  elif card_counts["J"] == 1:
    return HandType.PAIR
  return HandType.HIGH_CARD


CARD_VALUE = {"A": 14, "K": 13, "Q": 12, "J": 11, "T": 10}
for i in range(2, 10):
  CARD_VALUE[str(i)] = i
  
@dataclass
class Hand:
  cards: str
  bet: int
  evaluate_type: Callable[[str], HandType]
  
  @property
  def type(self) -> HandType:
    return self.evaluate_type(self.cards)
  
  def __lt__(self, other: 'Hand') -> bool:
    if self.type != other.type:
      return self.type < other.type
    
    for card_a, card_b in zip(self.cards, other.cards):
      if card_a != card_b:
        return CARD_VALUE[card_a] < CARD_VALUE[card_b]
      
    return False  # cards are equal
    
    

@register(2023, 7, 1, test=(test_game, 6440))
def part_one(lines: list[str]):
  hands: list[Hand] = []
  for line in lines:
    parts = line.split(" ")
    hands.append(Hand(parts[0], int(parts[1]), evaluate_hand_type))
  
  hands = sorted(hands)
  winnings = sum((i+1) * hand.bet for i, hand in enumerate(hands))
  return winnings

@register(2023, 7, 2, test=(test_game, 5905))
def part_two(lines: list[str]):
  CARD_VALUE["J"] = 1
  
  hands: list[Hand] = []
  for line in lines:
    parts = line.split(" ")
    hands.append(Hand(parts[0], int(parts[1]), evaluate_hand_type_with_joker))
    
  hands = sorted(hands)
  winnings = sum((i+1) * hand.bet for i, hand in enumerate(hands))
  return winnings
  
