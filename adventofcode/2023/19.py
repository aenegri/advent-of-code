from dataclasses import dataclass
from enum import StrEnum
import math
import re
from typing import NamedTuple

from adventofcode.registry import register

test_data = """
px{a<2006:qkq,m>2090:A,rfg}
pv{a>1716:R,A}
lnx{m>1548:A,A}
rfg{s<537:gd,x>2440:R,A}
qs{s>3448:A,lnx}
qkq{x<1416:A,crn}
crn{x>2662:A,R}
in{s<1351:px,qqz}
qqz{s>2770:qs,m<1801:hdj,R}
gd{a>3333:R,R}
hdj{m>838:A,pv}

{x=787,m=2655,a=1222,s=2876}
{x=1679,m=44,a=2067,s=496}
{x=2036,m=264,a=79,s=2244}
{x=2461,m=1339,a=466,s=291}
{x=2127,m=1623,a=2188,s=1013}
"""

class Span(NamedTuple):
  """Span where start and end points are included"""
  start: int
  end: int
  
  def __len__(self) -> int:
    return (self.end - self.start) + 1
  
  def split(self, x: int) -> tuple['Span']:
    if x <= self.start or x >= self.end:
      raise Exception(f"Invalid split at {x}")
    return Span(self.start, x - 1), Span(x, self.end)

class Category(StrEnum):
  X = "x"
  M = "m"
  A = "a"
  S = "s"
  
class Op(StrEnum):
  LT = "<"
  GT = ">"

@dataclass(frozen=True)
class Rule:
  category: Category
  op: Op
  value: int
  dest: str
  
  @classmethod
  def from_str(cls, text: str):
    m = re.match(r"([xmas])([<>])(\d+):(\w+)", text)
    cat = Category(m.group(1))
    op = Op(m.group(2))
    value = int(m.group(3))
    dest = m.group(4)
    return cls(cat, op, value, dest)
  
  def evaluate(self, part: dict[Category, int]) -> bool:
    if self.op == Op.LT:
      return part[self.category] < self.value
    elif self.op == Op.GT:
      return part[self.category] > self.value

@dataclass
class Workflow:
  id: str
  rules: list[Rule]
  default: str
  
  def destination(self, part: dict[Category, int]) -> str:
    for rule in self.rules:
      if rule.evaluate(part):
        return rule.dest
    return self.default
  
class System:
  workflows: dict[str, Workflow]
  parts: list[dict[Category, int]]
  
  def __init__(self, workflows: dict[str, Workflow], parts: list[dict[Category, int]]):
    self.workflows = workflows
    self.parts = parts
  
  @classmethod
  def from_str(cls, data: str):
    workflows = {}
    chunks = data.split("\n\n")
    for line in chunks[0].split("\n"):
      m = re.match(r"(\w+){(.+)}", line)
      id = m.group(1)
      conds = m.group(2).split(",")
      default = conds[-1]
      rules = [Rule.from_str(data) for data in conds[:-1]]
      workflows[id] = Workflow(id, rules, default)
      
    parts = []
    for line in chunks[1].split("\n"):
      m = re.match(r"{x=(\d+),m=(\d+),a=(\d+),s=(\d+)}", line)
      parts.append({
        Category.X: int(m.group(1)),
        Category.M: int(m.group(2)),
        Category.A: int(m.group(3)),
        Category.S: int(m.group(4)),
      })
    return cls(workflows, parts)
  
  def accepted(self, part: dict[Category, int]) -> bool:
    dest = "in"
    while dest not in ("A", "R"):
      flow = self.workflows[dest]
      dest = flow.destination(part)
    return dest == "A"
  
  def accepted_rating_sum(self) -> int:
    return sum(sum(part.values()) for part in self.parts if self.accepted(part))
  
  def distinct_combinations(self, dest: str, spans: tuple[Span]) -> int:
    if dest == "A":
      return math.prod(len(span) for span in spans)
    elif dest == "R":
      return 0
    
    flow = self.workflows[dest]
    span_idx = {Category.X: 0, Category.M: 1, Category.A: 2, Category.S: 3}
    
    for rule in flow.rules:
      span = spans[span_idx[rule.category]]
      
      if (rule.op == Op.LT and span.start >= rule.value) or (rule.op == Op.GT and span.end <= rule.value):
        continue  # rule doesn't apply
      elif (rule.op == Op.LT and span.end < rule.value) or (rule.op == Op.GT and span.start > rule.value):
        return self.distinct_combinations(rule.dest, spans)  # rule applies to all
      elif rule.op == Op.LT:
        split_spans = span.split(rule.value)
        spans_lower = [*spans]
        spans_higher = [*spans]
        spans_lower[span_idx[rule.category]] = split_spans[0]
        spans_higher[span_idx[rule.category]] = split_spans[1]
        return self.distinct_combinations(rule.dest, tuple(spans_lower)) + self.distinct_combinations(dest, tuple(spans_higher))
      else:
        split_spans = span.split(rule.value + 1)
        spans_lower = [*spans]
        spans_higher = [*spans]
        spans_lower[span_idx[rule.category]] = split_spans[0]
        spans_higher[span_idx[rule.category]] = split_spans[1]
        return self.distinct_combinations(dest, tuple(spans_lower)) + self.distinct_combinations(rule.dest, tuple(spans_higher))
      
    return self.distinct_combinations(flow.default, spans)
      

  
@register(2023, 19, 1, test=(test_data, 19114))
def part_one(lines: list[str]):
  data = "\n".join(lines)
  system = System.from_str(data)
  return system.accepted_rating_sum()

@register(2023, 19, 2, test=(test_data, 167409079868000))
def part_two(lines: list[str]):
  data = "\n".join(lines)
  system = System.from_str(data)
  return system.distinct_combinations("in", (Span(1, 4000), Span(1, 4000), Span(1, 4000), Span(1, 4000)))
