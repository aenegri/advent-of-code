import itertools
from adventofcode.registry import register


test_image = """
...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....
"""

EXPANSION_MULTIPLIER = 1

class Coordinate:
  row: int
  col: int
  row_expansions: int
  col_expansions: int
  
  def __init__(self, row: int, col: int):
    self.row = row
    self.col = col
    self.row_expansions = 0
    self.col_expansions = 0
    
  def __repr__(self) -> str:
    return f"Coordinate {self.row=} {self.col=} {self.row_expansions=} {self.col_expansions=}"

  @property
  def absolute_row(self) -> int:
    return self.row + self.row_expansions * (EXPANSION_MULTIPLIER - 1)
  
  @property
  def absolute_col(self) -> int:
    return self.col + self.col_expansions * (EXPANSION_MULTIPLIER - 1)
  
  def cartesian_distance(self, other: 'Coordinate') -> int:
    return (abs(other.absolute_row - self.absolute_row) +
      + abs(other.absolute_col - self.absolute_col))
    
    
def expand(image: list[list[str]], galaxies: list[Coordinate]) -> None:
  for y, row in enumerate(image):
    if all(pixel == "." for pixel in row):
      for galaxy in galaxies:
        if galaxy.row > y:
          galaxy.row_expansions += 1
  
  for x in range(len(image[0])):
    if all(row[x] == "." for row in image):
      for galaxy in galaxies:
        if galaxy.col > x:
          galaxy.col_expansions += 1


def find_galaxies(image: list[list[str]]) -> list[Coordinate]:
  galaxies = []
  for y, row in enumerate(image):
    for x, pixel in enumerate(row):
      if pixel == "#":
        galaxies.append(Coordinate(y, x))
  return galaxies


@register(2023, 11, 1, test=(test_image, 374))
def part_one(lines: list[str]):
  image = [list(line) for line in lines]
  galaxies = find_galaxies(image)
  expand(image, galaxies)
  dists = [a.cartesian_distance(b) for a, b in itertools.combinations(galaxies, 2)]
  return sum(dists)

@register(2023, 11, 2)
def part_two(lines: list[str]):
  image = [list(line) for line in lines]
  galaxies = find_galaxies(image)
  expand(image, galaxies)
  global EXPANSION_MULTIPLIER
  EXPANSION_MULTIPLIER = 1000000
  dists = [a.cartesian_distance(b) for a, b in itertools.combinations(galaxies, 2)]
  return sum(dists)
