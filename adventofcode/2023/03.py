import collections
from typing import Optional
from adventofcode.registry import register

test_engine_schematic = """
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
"""

def adjacent_to_symbol(row: int, start_col: int, end_col: int, schematic: list[str]) -> Optional[tuple[int, int]]:
  search_area = [(row, start_col -1), (row, end_col)]
  search_area.extend((i, col) for col in range(start_col-1, end_col+1) for i in (row-1, row+1))
  
  for r, c in search_area:
    if r < 0 or r >= len(schematic) or c < 0 or c >= len(schematic[r]):
      continue  # out of bounds check
    candidate = schematic[r][c]
    if not candidate.isnumeric() and candidate != ".":
      return r, c
  return None
    

@register(2023, 3, 1, test=(test_engine_schematic, 4361))
def part_one(lines: list[str]):
  # extend schematic so we don't have to handle nums ending on right edge
  for i in range(len(lines)):
    lines[i] = lines[i] + "."
    
  part_nums = []
  for row, line in enumerate(lines):
    num = 0
    for col, char in enumerate(line):
      if char.isnumeric():
        num *= 10
        num += int(char)
        continue
      elif num == 0:
        continue
      
      col_start = col - len(str(num))
      if adjacent_to_symbol(row, col_start, col, lines) is not None:
        part_nums.append(num)
      num = 0
      
  return sum(part_nums)

@register(2023, 3, 2, test=(test_engine_schematic, 467835))
def part_two(lines: list[str]):
  # extend schematic so we don't have to handle nums ending on right edge
  for i in range(len(lines)):
    lines[i] = lines[i] + "."
    
  symbol_to_parts: dict[tuple[int, int], list[int]] = collections.defaultdict(list)
  for row, line in enumerate(lines):
    num = 0
    for col, char in enumerate(line):
      if char.isnumeric():
        num *= 10
        num += int(char)
        continue
      elif num == 0:
        continue
      
      col_start = col - len(str(num))
      if pos := adjacent_to_symbol(row, col_start, col, lines):
        symbol_to_parts[pos].append(num)
      num = 0
  
  gear_ratios = [parts[0] * parts[1] for symbol, parts in symbol_to_parts.items() if len(parts) == 2]
  
  return sum(gear_ratios)
