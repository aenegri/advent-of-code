import collections
from collections import deque
from enum import Enum, auto
from functools import cached_property
from math import lcm
import re
from typing import NamedTuple, Optional, Protocol
from adventofcode.registry import register

class Pulse(Enum):
  LO = auto()
  HI = auto()
  MANUAL = auto()
  
class Action(NamedTuple):
  start_id: str
  dest_id: str
  pulse: Pulse
  
class ProcessPulse(Protocol):
  def process(self, start_id: str, pulse: Pulse) -> Optional[list[Action]]: ...

class FlipFlopModule:
  id: str
  edges: list[str]
  active: bool
  
  def __init__(self, id: str, edges: list[str]):
    self.id = id
    self.edges = edges
    self.active = False
    
  def __hash__(self) -> int:
    return int(self.active)
  
  def process(self, start_id: str, pulse: Pulse) -> Optional[list[Action]]:
    if pulse == Pulse.HI:
      return None
    self.active = not self.active
    
    if self.active:
      return [Action(self.id, edge, Pulse.HI) for edge in self.edges]
    return  [Action(self.id, edge, Pulse.LO) for edge in self.edges]
  
class ConjunctionModule:
  id: str
  edges: list[str]
  _in_edges: list[str]
  mem_lo: set[str]
  mem_hi: set[str]
  
  def __init__(self, id: str, edges: list[str], in_edges: list[str]):
    self.id = id
    self.edges = edges
    self._in_edges = in_edges
    self.mem_hi = set()
    
  @cached_property
  def mem_lo(self):
    return {e for e in self._in_edges}
    
  def process(self, start_id: str, pulse: Pulse) -> Optional[list[Action]]:
    if start_id in self.mem_lo and pulse == Pulse.HI:
      self.mem_lo.remove(start_id)
      self.mem_hi.add(start_id)
    elif start_id in self.mem_hi and pulse == Pulse.LO:
      self.mem_hi.remove(start_id)
      self.mem_lo.add(start_id)
      
    out_pulse = Pulse.LO if not self.mem_lo else Pulse.HI
    return [Action(self.id, edge, out_pulse) for edge in self.edges]
  
class BroadcastModule:
  id: str
  edges: list[str]
  
  def __init__(self, edges: list[str]):
    self.id = "broadcaster"
    self.edges = edges    
  
  def process(self, start_id: str, pulse: Pulse) -> Optional[list[Action]]:
    return [Action(self.id, edge, pulse) for edge in self.edges]
  
class ButtonModule:
  id: str
  
  def __init__(self):
    self.id = "button"
  
  def process(self, start_id: str, pulse: Pulse) -> Optional[list[Action]]:
    return [Action(self.id, "broadcaster", Pulse.LO)]
  
class NoopModule:
  id: str
  
  def __init__(self, id: str):
    self.id = id
    
  def process(self, start_id: str, pulse: Pulse) -> Optional[list[Action]]:
    return None
  
class Simulator:
  modules: dict[str, ProcessPulse]  
  cycles: int
  conjunction_cycles: dict[str, int]
  
  def __init__(self, modules: dict[str, ProcessPulse]):
    self.modules = modules
    self.cycles = 0
    self.conjunction_cycles = collections.defaultdict(lambda: 0)
  
  @classmethod
  def from_lines(cls, lines: list[str]):
    modules: dict[str, ProcessPulse] = collections.defaultdict(lambda: NoopModule("noop"))
    modules["button"] = ButtonModule()
    in_edges = collections.defaultdict(list)
    module_pattern = re.compile(r"([&%]?)(\w+)\s->\s(.+)")
    for line in lines:
      m = module_pattern.match(line)
      module_type = m.group(1)
      module_id = m.group(2)
      out_edges = m.group(3).split(", ")
      
      for edge in out_edges:
        in_edges[edge].append(module_id)
      
      if module_type == "%":
        modules[module_id] = FlipFlopModule(module_id, out_edges)
      elif module_type == "&":
        # pass inbound edges list, will get updated since it is mutable
        modules[module_id] = ConjunctionModule(module_id, out_edges, in_edges[module_id])
      elif module_id == "broadcaster":
        modules[module_id] = BroadcastModule(out_edges)
      else:
        raise Exception(f"Unknown module {module_type=} {module_id=}")
    return cls(modules)
  
  def push_button(self, times: int) -> tuple[int, int]:
    lo_count, hi_count = 0, 0
    for i in range(times):
      counts = self._push_button()
      lo_count += counts[0]
      hi_count += counts[1]
    return lo_count, hi_count
  
  def min_presses(self, dest_id: str, pulse: Pulse) -> int:    
    conjunction_roots = ["nx", "jq", "cc", "sp"]
    for _ in range(5000):
      self._push_button()
      self.cycles += 1
    cycles = [self.conjunction_cycles[root] for root in conjunction_roots]
    return lcm(*[self.conjunction_cycles[root] for root in conjunction_roots])
  
  def _push_button(self) -> tuple[int, int]:
    lo_count, hi_count = 0, 0
    
    queue: deque[Action] = deque()
    queue.append(Action("", "button", Pulse.MANUAL))
    
    while queue:
      curr_action = queue.popleft()
      
      if curr_action.pulse == Pulse.LO:
        lo_count += 1
      elif curr_action.pulse == Pulse.HI:
        hi_count += 1
        
      module = self.modules[curr_action.dest_id]
      
      if isinstance(module, ConjunctionModule) and curr_action.pulse == Pulse.LO and module.id not in self.conjunction_cycles:
        self.conjunction_cycles[module.id] = self.cycles + 1
        
      additional_actions = module.process(curr_action.start_id, curr_action.pulse)
      
      if additional_actions is not None:
        queue.extend(additional_actions)
    
    
    return lo_count, hi_count

test_simple = (r"""
broadcaster -> a, b, c
%a -> b
%b -> c
%c -> inv
&inv -> a
""", 32000000)

test_advanced = (r"""
broadcaster -> a
%a -> inv, con
&inv -> b
%b -> con
&con -> output
""", 11687500)

test_reddit = (r"""
broadcaster -> aa, aa
%aa -> bb
&bb -> rx
""", 0)

@register(2023, 20, 1, test=test_advanced)
def part_one(lines: list[str]):
  sim = Simulator.from_lines(lines)
  lo, hi = sim.push_button(1000)
  return lo * hi

@register(2023, 20, 2)
def part_two(lines: list[str]):
  sim = Simulator.from_lines(lines)
  return sim.min_presses("rx", Pulse.LO)
