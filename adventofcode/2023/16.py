from collections import deque
from adventofcode.graphs import DOWN, LEFT, RIGHT, UP, Direction, Matrix, Point

from adventofcode.registry import register

test_cave = r"""
.|...\....
|.-.\.....
.....|-...
........|.
..........
.........\
..../.\\..
.-.-/..|..
.|....-|.\
..//.|....
"""

def bfs(matrix: Matrix[Point, str], start: tuple[Point, Direction]) -> set[Point]:
  visited: set[tuple[Point, Direction]] = set()
  queue: deque[tuple[Point, Direction]] = deque([(start)])
  next_level: deque[tuple[Point, Direction]] = deque()
  
  while queue:
    while queue:
      pt, dir = queue.popleft()
      if (pt, dir) in visited or pt not in matrix:
        continue
      
      visited.add((pt, dir))
      curr = matrix[pt]
      #print(f"Visiting {pt=} {dir=} and {curr=}")
      
      if curr == "|" and dir in (LEFT, RIGHT):
        next_level.append((pt + UP, UP))
        next_level.append((pt + DOWN, DOWN))
      elif curr == "-" and dir in (UP, DOWN):
        next_level.append((pt + LEFT, LEFT))
        next_level.append((pt + RIGHT, RIGHT))
      elif curr == "/":
        bends = {UP: RIGHT, RIGHT: UP, DOWN: LEFT, LEFT: DOWN}
        next_level.append((pt + bends[dir], bends[dir]))
      elif curr == "\\":
        bends = {UP: LEFT, LEFT: UP, DOWN: RIGHT, RIGHT: DOWN}
        next_level.append((pt + bends[dir], bends[dir]))
      else:
        next_level.append((pt + dir, dir))
    queue = next_level
    next_level = deque()
  
  return set(pt for pt, _ in visited)

@register(2023, 16, 1, test=(test_cave, 46))
def part_one(lines: list[str]):
  matrix = Matrix.from_lines(lines)
  energized = bfs(matrix, start=(Point(0, 0), RIGHT))
  return len(energized)
  

@register(2023, 16, 2, test=(test_cave, 51))
def part_two(lines: list[str]):
  matrix = Matrix.from_lines(lines)
  starts = []
  for x in range(matrix.width):
    starts.append((Point(x, 0), DOWN))
    starts.append((Point(x, matrix.height - 1), UP))
  for y in range(matrix.height):
    starts.append((Point(0, y), RIGHT))
    starts.append((Point(matrix.width - 1, y), LEFT))
  
  return max(len(bfs(matrix, start=start)) for start in starts)
