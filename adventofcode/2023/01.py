import re
from adventofcode.parse import digits
from adventofcode.registry import register

part_one_test_data = """
1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet
""".strip().split("\n")

@register(2023, 1, 1, test=(part_one_test_data, 142))
def part_one(lines: list[str]) -> int:
  calibration_ints = [digits(l) for l in lines]
  
  calibration_sum = sum((l[0]*10 + l[-1] for l in calibration_ints))
  
  return calibration_sum

part_two_test_data = """
two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen
""".strip().split("\n")

@register(2023, 1, 2, test=(part_two_test_data, 281))
def part_two(lines: list[str]):
  calibration_sum = 0
  word_to_digit = {"one": 1, "two": 2, "three": 3, "four": 4, "five": 5, "six": 6, "seven": 7, "eight": 8, "nine": 9}
  
  for l in lines:
    nums = []
    for m in re.finditer(r"\d", l):
      nums.append((m.start(), int(m.group(0))))
    for word, digit in word_to_digit.items():
      for m in re.finditer(word, l):
        nums.append((m.start(), digit))
    sorted_nums = sorted(nums, key=lambda x: x[0])
    calibration_sum += sorted_nums[0][1]*10 + sorted_nums[-1][1]
  
  return calibration_sum