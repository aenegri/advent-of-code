
import heapq
from typing import NamedTuple
from adventofcode.graphs import DIRECT, DOWN, LEFT, RIGHT, UP, Direction, Point
from adventofcode.registry import register

test_map = """
2413432311323
3215453535623
3255245654254
3446585845452
4546657867536
1438598798454
4457876987766
3637877979653
4654967986887
4564679986453
1224686865563
2546548887735
4322674655533
"""


class SearchState(NamedTuple):
  cost: int
  dir: Direction
  consecutive: int
  point: Point

def djikstra(graph: dict[Point, int], target: Point, *, min_consecutive: int = 0, max_consecutive: int = 3):
  # priority queue (min heap) with (cost, dir, consecutive, point)
  start = SearchState(0, RIGHT, 0, Point(0, 0))
  heap: list[SearchState] = [start]
  # visited state does not need cost - djikstra ensures the lowest cost path is hit first
  visited: set[tuple[Direction, int, Point]] = set()
  
  opposites = {RIGHT: LEFT, LEFT: RIGHT, UP: DOWN, DOWN: UP}
  
  while heap:
    state = heapq.heappop(heap)
    cost, dir, consecutive, point = state
    
    if point == target and consecutive >= min_consecutive:
      return cost
    elif point == target:
      continue
    
    # figure out valid next steps    
    if consecutive < min_consecutive:
      opts = {dir}
    else:
      exclude = {opposites[dir]}
      if consecutive == max_consecutive:
        exclude.add(dir)
      opts = DIRECT.difference(exclude)
    
    # add options to the heap
    for opt in opts:
      neighbor = point + opt
      if neighbor not in graph:
        continue
      neighbor_cost = cost + graph[neighbor]
      neighbor_consecutive = consecutive + 1 if opt == dir else 1
      candidate = (opt, neighbor_consecutive, neighbor)
      if candidate in visited:
        continue
      heapq.heappush(heap, SearchState(neighbor_cost, opt, neighbor_consecutive, neighbor))
      visited.add(candidate)
  
  raise Exception(f"Did not find target {target}")
  

@register(2023, 17, 1, test=(test_map, 102))
def part_one(lines: list[str]):
  graph: dict[Point, int] = {}
  for y, line in enumerate(lines):
    for x, element in enumerate(line):
      graph[Point(x, y)] = int(element)
      
  target = Point(len(lines[0]) - 1, len(lines) - 1)
  shortest_path = djikstra(graph, target)
  return shortest_path
  
  

@register(2023, 17, 2, test=(test_map, 94))
def part_two(lines: list[str]):
  graph: dict[Point, int] = {}
  for y, line in enumerate(lines):
    for x, element in enumerate(line):
      graph[Point(x, y)] = int(element)
      
  target = Point(len(lines[0]) - 1, len(lines) - 1)
  shortest_path = djikstra(graph, target, min_consecutive=4, max_consecutive=10)
  return shortest_path
