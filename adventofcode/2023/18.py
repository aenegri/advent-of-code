import itertools
import re
from typing import NamedTuple, Sequence
from adventofcode.graphs import DOWN, LEFT, RIGHT, UP, Direction, Matrix, Point

from adventofcode.registry import register

test_dig_plan = """
R 6 (#70c710)
D 5 (#0dc571)
L 2 (#5713f0)
D 2 (#d2c081)
R 2 (#59c680)
D 2 (#411b91)
L 5 (#8ceee2)
U 2 (#caa173)
L 1 (#1b58a2)
U 2 (#caa171)
R 2 (#7807d2)
U 3 (#a77fa3)
L 2 (#015232)
U 2 (#7a21e3)
"""

class Instruction(NamedTuple):
  dir: Direction
  steps: int
  
def dig(instructions: list[Instruction]) -> tuple[Matrix[str], int]:
  grid: Matrix[str] = Matrix()
  curr = Point(0, 0)
  grid[curr] = "#"
  perimeter = 0
  for dir, steps in instructions:
    perimeter += steps
    curr += (dir[0] * steps, dir[1] * steps)
    grid[curr] = "#"
  return grid, perimeter

def shoelace(vertices: Sequence[Point]) -> float:
  """
  Calculate the area of the polygon using the shoelace formula
  https://en.wikipedia.org/wiki/Shoelace_formula
  """
  area = 0
  for (x0, y0), (x1, y1) in itertools.pairwise(vertices):
    area += (x0 * y1) - (y0 * x1)
  return area / 2.0

def picks(grid: Matrix[str], perimeter: int) -> float:
  """
  Calculate area, inclusive of edges
  https://en.wikipedia.org/wiki/Pick's_theorem
    
  A: total area (given by shoelace formula)
  i: interior area
  b: perimeter
  
  What we want is i + b
  
  [Picks] A = i + b/2 - 1 --> i + b = A + b/2 + 1
  """
  vertices = list(grid.keys())
  vertices.append(Point(0, 0))
  return shoelace(vertices) + (perimeter / 2.0) + 1
  

@register(2023, 18, 1, test=(test_dig_plan, 62))
def part_one(lines: list[str]):
  dirs = {"U": UP, "D": DOWN, "L": LEFT, "R": RIGHT}
  instructions = []
  for line in lines:
    m = re.match(r"(\w)\s+(\d+)\s+\(#([\d\w]{6})\)", line)
    dir = dirs[m.group(1)]
    steps = int(m.group(2))
    instructions.append(Instruction(dir, steps))
  grid, perimeter = dig(instructions)
  area = picks(grid, perimeter)
  return int(area)

@register(2023, 18, 2, test=(test_dig_plan, 952408144115))
def part_two(lines: list[str]):
  dirs = {0: RIGHT, 1: DOWN, 2: LEFT, 3: UP}
  instructions = []
  for line in lines:
    m = re.match(r"\w\s+\d+\s+\(#([\d\w]{5})([\d\w])\)", line)
    dir = dirs[int(m.group(2))]
    steps = int(m.group(1), base=16)
    instructions.append(Instruction(dir, steps))
  grid, perimeter = dig(instructions)
  area = picks(grid, perimeter)
  return int(area)
