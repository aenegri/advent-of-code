import collections
import re

from adventofcode.registry import register

sample_games = """
Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
"""

colors = {"red", "green", "blue"}

def parse_games(lines: list[str]) -> dict[int, list[dict[str, int]]]:
  games = collections.defaultdict(list)
  for l in lines:
    game_id_match = re.match(r"Game (\d+)", l)
    game_id = int(game_id_match.group(1))
        
    rounds = l.split(";")
    for r in rounds:
      cubes = {}
      for c in colors:
        cube_match = re.search(r"(\d+) " + c, r)
        if cube_match is not None:
          cubes[c] = int(cube_match.group(1))
      games[game_id].append(cubes)
  
  return games

def is_game_valid(rounds: list[dict[str, int]], bag: dict[str, int]):
  for r in rounds:
    for color, count in r.items():
      if bag[color] < count:
        return False
      
  return True
          

@register(2023, 2, 1, test=(sample_games, 8))
def part_one(lines: list[str]) -> int:
  bag = {"red": 12, "green": 13, "blue": 14}
  games = parse_games(lines)
  valid_sum = 0
  for game_id, rounds in games.items():
    if is_game_valid(rounds, bag):
      valid_sum += game_id
  return valid_sum

@register(2023, 2, 2, test=(sample_games, 2286))
def part_two(lines: list[str]):
  games = parse_games(lines)
  power_sum = 0
  for rounds in games.values():
    bag_mins = {"red": None, "green": None, "blue": None}
    for round in rounds:
      for color, count in round.items():
        if bag_mins[color] is None:
          bag_mins[color] = count
        else:
          bag_mins[color] = max(bag_mins[color], count)
    
    prod = 1
    for color, min in bag_mins.items():
      prod *= min
    power_sum += prod
    
  return power_sum