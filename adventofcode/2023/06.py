import math
import re

from adventofcode.parse import ints
from adventofcode.registry import register

test_races = """
Time:      7  15   30
Distance:  9  40  200
"""

def roots(t: int, d: int) -> tuple[float]:
  """
  Hard-coded quadratic formula, based on the following equations:
  [ x + y = t, x * y > d ]
  """
  root_section = math.sqrt((t ** 2) - (4*d))
  root_lo = (t - root_section) / 2
  root_hi = (t + root_section) / 2
  return root_lo, root_hi
  
def solution_count(t: int, d: int) -> int:
  root_lo, root_hi = roots(t, d)
  start, end = math.ceil(root_lo), math.floor(root_hi)
  count = end - start + 1
  
  # if the calculated roots match the prior distance, discard
  matches_prior_record = start * (t - start) == d
  if matches_prior_record:
    count -= 2
    
  return count

@register(2023, 6, 1, test=(test_races, 288))
def part_one(lines: list[str]):
  times, distances = ints(lines[0]), ints(lines[1])
  return math.prod(solution_count(t, d) for t, d in zip(times, distances))
  

def extract_int(line: str) -> int:
  num = ""
  matches = re.finditer(r"\d", line)
  for match in matches:
    num += match.group(0)
  return int(num)

@register(2023, 6, 2, test=(test_races, 71503))
def part_two(lines: list[str]):
  time, distance = extract_int(lines[0]), extract_int(lines[1])
  return solution_count(time, distance)
