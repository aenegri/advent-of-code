
from dataclasses import dataclass
from adventofcode.registry import register

test_pipe_maze = """
..F7.
.FJ|.
SJ.L7
|F--J
LJ...
"""

PIPE_CONNECTIONS = {
  "|": ((-1, 0), (1, 0)),
  "-": ((0, -1), (0, 1)),
  "L": ((-1, 0), (0, 1)),
  "J": ((0, -1), (-1, 0)),
  "7": ((0, -1), (1, 0)),
  "F": ((0, 1), (1, 0)),
}

class PipeMaze:
  tiles: list[str]
  loop_members: set[tuple[int, int]]
  
  def __init__(self, tiles: list[str]):
    self.tiles = tiles
    self.loop_members = set()
    
  
  def start(self) -> tuple[int, int]:
    for i, row in enumerate(self.tiles):
      for j, pipe in enumerate(row):
        if pipe == "S":
          return (i, j)
    raise Exception("Start pipe not found")
  
  def traverse_loop(self) -> int:
    pos = self.start()
    prev = None
    steps = 0
    
    # at start, advance to first neighbor that connects back
    for dy, dx in ((0, -1), (0, 1), (-1, 0), (1, 0)):
      conn_pos = (pos[0] + dy, pos[1] + dx)
      pipe = self.tiles[conn_pos[0]][conn_pos[1]]
      for dy, dx in PIPE_CONNECTIONS[pipe]:
        if (conn_pos[0] + dy, conn_pos[1] + dx) == pos:
          prev = pos
          pos = conn_pos
          break
      if prev is not None:
        break
      
    self.loop_members.add(prev)
    self.loop_members.add(pos)
    
    steps = 1
    while self.tiles[pos[0]][pos[1]] != "S":
      pipe = self.tiles[pos[0]][pos[1]]
      for dy, dx in PIPE_CONNECTIONS[pipe]:
        conn_pos = (pos[0] + dy, pos[1] + dx)
        if conn_pos != prev:
          prev = pos
          pos = conn_pos
          self.loop_members.add(pos)
          break
      steps += 1
    
    return steps

  
  def enclosed_count(self) -> int:
    # swap "S" with the actual pipe
    start = self.start()
    connects_above = self.tiles[start[0] - 1][start[1]] in ("|", "7", "F")
    connects_below = self.tiles[start[0] - 1][start[1]] in ("|", "L", "J")
    connects_left = self.tiles[start[0]][start[1] - 1] in ("-", "L", "F")
    connects_right = self.tiles[start[0]][start[1] + 1] in ("-", "7", "J")
    
    replacement = ""
    if connects_above and connects_below:
      replacement = "|"
    elif connects_above and connects_left:
      replacement = "J"
    elif connects_above and connects_right:
      replacement = "L"
    elif connects_below and connects_left:
      replacement = "7"
    elif connects_below and connects_right:
      replacement = "F"
    elif connects_left and connects_right:
      replacement = "-"
    
    row = list(self.tiles[start[0]])
    row[start[1]] = replacement
    self.tiles[start[0]] = "".join(row)
    count = 0
    for y, row in enumerate(self.tiles):
      inside = False
      x = 0
      while x < len(row):
        if (y, x) not in self.loop_members:
          count += int(inside)
          x += 1
          continue
        
        # update inside state when we cross in/out of the main loop
        if row[x] == "|":
          inside = not inside
          x += 1
        elif row[x] in ("F", "L"):
          start_tile = row[x]
          x += 1
          while row[x] not in ("J", "7"):
            x += 1
          if (start_tile, row[x]) in (("F", "J"), ("L", "7")):
            inside = not inside
          x += 1

    return count
          
        
        
        
    

@register(2023, 10, 1, test=(test_pipe_maze, 8))
def part_one(lines: list[str]):
  maze = PipeMaze(lines)
  loop_distance = maze.traverse_loop()
  return loop_distance // 2
  
  
@register(2023, 10, 2, test=("""
FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ7F7FJ-
L---JF-JLJ.||-FJLJJ7
|F|F-JF---7F7-L7L|7|
|FFJF7L7F-JF7|JL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L
""", 10))
def part_two(lines: list[str]):
  maze = PipeMaze(lines)
  maze.traverse_loop()
  return maze.enclosed_count()
