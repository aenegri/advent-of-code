import functools
import itertools
from adventofcode.parse import ints
from adventofcode.registry import register

test_oasis_report = """
0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45
"""

def calc_differences(history: list[int]) -> list[list[int]]:
  sequences: list[list[int]] = [history]
  all_zero = False
  while not all_zero:
    row = sequences[-1]
    below = [b - a for a, b in itertools.pairwise(row)]
    sequences.append(below)
    all_zero = all(num == 0 for num in below)
  return sequences


def predict_next(history: list[int]) -> int:
  sequences = calc_differences(history)
  return sum(sequence[-1] for sequence in sequences)


def predict_prev(history: list[int]) -> int:
  sequences = calc_differences(history)
  sequences.reverse()
  prev = functools.reduce(lambda x, y: y - x, (sequence[0] for sequence in sequences))
  return prev
  

@register(2023, 9, 1, test=(test_oasis_report, 114))
def part_one(lines: list[str]):
  return sum(predict_next(ints(line)) for line in lines)
  

@register(2023, 9, 2, test=(test_oasis_report, 2))
def part_two(lines: list[str]):
  return sum(predict_prev(ints(line)) for line in lines)
