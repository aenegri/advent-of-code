import math
import re

from adventofcode.registry import register

test_map = """
LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)
"""

test_ghost_map = """
LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)
"""

  
def build_network(lines: list[str]) -> dict[str, tuple[str, str]]:
  network = {}
  for line in lines:
    m = re.match(r"(\w+)\s=\s\((\w+),\s(\w+)\)", line)
    id, left, right = m.group(1), m.group(2), m.group(3)
    network[id] = left, right
  return network

@register(2023, 8, 1, test=(test_map, 6))
def part_one(lines: list[str]):
  instructions = lines[0]
  network = build_network(lines[2:])
  
  curr = "AAA"
  instruction_ptr = 0
  steps = 0
  while curr != "ZZZ":
    dir = instructions[instruction_ptr]
    
    if dir == "L":
      curr = network[curr][0]
    else:
      curr = network[curr][1]
    
    steps += 1
    instruction_ptr = (instruction_ptr + 1) % len(instructions)
  
  return steps

@register(2023, 8, 2, test=(test_ghost_map, 6))
def part_two(lines: list[str]):
  instructions = lines[0]
  network = build_network(lines[2:])
  
  start_positions = [id for id in network if id[-1] in ("A", "Z")]
  dist_to_end = {}
  for position in start_positions:
    curr = position
    instruction_ptr, steps = 0, 0
    while curr[-1] != "Z" or steps == 0:
      dir = instructions[instruction_ptr]
      
      if dir == "L":
        curr = network[curr][0]
      else:
        curr = network[curr][1]
      
      steps += 1
      instruction_ptr = (instruction_ptr + 1) % len(instructions)
      
    dist_to_end[position] = steps
      
  steps = math.lcm(*dist_to_end.values())
  
  return steps
