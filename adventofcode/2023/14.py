
import collections
from enum import Enum
from typing import NamedTuple
from adventofcode.registry import register

test_platform = """
O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....
"""

class Tile(Enum):
  EMPTY = "."
  CUBE_ROCK = "#"
  ROUND_ROCK = "O"

class Coordinate(NamedTuple):
  row: int
  col: int

class Platform:
  tiles: dict[Coordinate, Tile]
  round_rocks: list[Coordinate]
  height: int
  width: int
  
  def __init__(
    self,
    tiles: dict[Coordinate, Tile],
    round_rocks: list[Coordinate],
    height: int,
    width: int,
  ):
    self.tiles = tiles
    self.round_rocks = round_rocks
    self.height = height
    self.width = width
    
  def __repr__(self) -> str:
    lines = []
    for y in range(self.height):
      lines.append("".join(self.tiles[(y, x)].value for x in range(self.width)))
    return "\n".join(lines)
  
  def __hash__(self) -> str:
    """Hash based on round rock positions"""
    return hash(",".join(str(coord) for coord in sorted(self.round_rocks)))
      
  
  @classmethod
  def from_lines(cls, lines: list[str]):
    tiles = {}
    round_rocks = []
    for y, row in enumerate(lines):
      for x, cell in enumerate(row):
        coord, tile = Coordinate(y, x), Tile(cell)
        tiles[coord] = tile
        if tile == Tile.ROUND_ROCK:
          round_rocks.append(coord)
    return cls(tiles, round_rocks, len(lines), len(lines[0]))
  
  def cycle(self, n: int) -> int:
    """
    Returns the load after running n cycles
    
    Detects loops by recording board state after each cycle
    """
    cycles = 0
    states: dict[str, int] = {}
    prev_states: list[str] = []
    while cycles < n:
      for dir in (Coordinate(-1, 0), Coordinate(0, -1), Coordinate(1, 0), Coordinate(0, 1)):
        self.tilt(dir)
      cycles += 1
      curr_state = hash(self)
      if curr_state not in states:
        states[curr_state] = cycles
        prev_states.append(curr_state)
        continue
      
      # if we've been here before, loop back until back at the current state
      i = len(prev_states) - 1
      while prev_states[i] != curr_state:
        i -= 1
        
      loop_length = cycles - i
      if (n - i - 3) % loop_length == 0:
        return self.load()
      
  def in_bounds(self, coord: Coordinate) -> bool:
    return (
      coord.row >= 0 and coord.col >= 0
      and coord.row < self.height and coord.col < self.width
    )
  
  def tilt(self, dir: Coordinate) -> None:
    """Tilt the round rocks"""
    # we need to go in order from the edge we are tilting towards
    reverse = dir in ((1, 0), (0, 1))
    if dir in ((-1, 0), (1, 0)):
      self.round_rocks = sorted(self.round_rocks, key=lambda c: c[0], reverse=reverse)
    else:
      self.round_rocks = sorted(self.round_rocks, key=lambda c: c[1], reverse=reverse)
      
    updated_round_rocks = []
    for start_coord in self.round_rocks:
      y, x = start_coord.row + dir.row, start_coord.col + dir.col
      while self.in_bounds(Coordinate(y, x)) and self.tiles[(y, x)] == Tile.EMPTY:
        y += dir.row
        x += dir.col
        
      # go back one step - easier than handling this in while condition
      y -= dir.row
      x -= dir.col
      
      end_coord = Coordinate(y, x)
      updated_round_rocks.append(end_coord)
      self.tiles[start_coord] = Tile.EMPTY
      self.tiles[end_coord] = Tile.ROUND_ROCK
    self.round_rocks = updated_round_rocks
      
  
  def load(self) -> int:
    return sum(self.height - coord.row for coord in self.round_rocks)
          

@register(2023, 14, 1, test=(test_platform, 136))
def part_one(lines: list[str]):
  platform = Platform.from_lines(lines)
  platform.tilt(Coordinate(-1, 0))
  return platform.load()

@register(2023, 14, 2, test=(test_platform, 64))
def part_two(lines: list[str]):
  platform = Platform.from_lines(lines)
  return platform.cycle(1000000)
