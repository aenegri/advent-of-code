from functools import cached_property
from typing import Generic, Mapping, NamedTuple, TypeVar, Union

Direction = tuple[int, int]


UP: Direction = (0, -1)
DOWN: Direction = (0, 1)
LEFT: Direction = (-1, 0)
RIGHT: Direction = (1, 0)

DIRECT: set[Direction] = {UP, DOWN, LEFT, RIGHT}

UP_LEFT: Direction = (-1, -1)
UP_RIGHT: Direction = (1, -1)
DOWN_LEFT: Direction = (-1, 1)
DOWN_RIGHT: Direction = (1, 1)

DIAGONALS: set[Direction] = {UP_LEFT, UP_RIGHT, DOWN_LEFT, DOWN_RIGHT}

ALL: tuple[Direction] = DIRECT.union(DIAGONALS)

class Point(NamedTuple):
  x: int
  y: int
  
  def __str__(self) -> str:
    return repr(self)
  
  def __repr__(self) -> str:
    return f"Point({self.x}, {self.y})"
  
  def __add__(self, other: Union['Point',tuple[int, int]]) -> 'Point':
    if isinstance(other, Point):
      return Point(self.x + other.x, self.y + other.y)
    elif isinstance(other, tuple):
      return Point(self.x + other[0], self.y + other[1])
    raise TypeError(f"Unsupported operand type: {type(other)}")

  def __sub__(self, other: Union['Point', tuple[int, int]]) -> 'Point':
    if isinstance(other, Point):
      return Point(self.x - other.x, self.y - other.y)
    elif isinstance(other, tuple):
      return Point(self.x - other[0], self.y - other[1])
    raise TypeError(f"Unsupported operand type: {type(other)}")
  
  def manhattan_distance(self, other: 'Point') -> int:
    return abs(other.x - self.x) + abs(other.y - self.y)
  

V = TypeVar("V", str, int, bool)


class Matrix(dict[Point, V], Generic[V]):
  def __init__(
    self,
    m: Mapping[Point, V] | None = None,
  ):
    if m is not None:
      super().__init__(m)
    else:
      super().__init__()
    
  @classmethod
  def from_lines(cls, lines: list[str]):
    data = {}
    for y, line in enumerate(lines):
      for x, cell in enumerate(line):
        data[Point(x, y)] = cell
    return cls(data)
  
  @property
  def height(self) -> int:
    lo, hi = self.ybounds
    return hi - lo + 1
  
  @property
  def width(self) -> int:
    lo, hi = self.xbounds
    return hi - lo + 1
  
  @cached_property
  def xbounds(self) -> tuple[int, int]:
    xs = [pt[0] for pt in self.keys()]
    return min(xs), max(xs)
  
  @cached_property
  def ybounds(self) -> tuple[int, int]:
    ys = [pt[1] for pt in self.keys()]
    return min(ys), max(ys)