from dataclasses import dataclass
from functools import wraps
from importlib import import_module
import os
from pathlib import Path
from typing import Callable, Optional

TestData = list[str] | str

@dataclass
class RegistryEntry:
  year: int
  day: int
  part: int
  solution: Callable[[list[str]], any]
  test: Optional[tuple[TestData, any]] = None

SOLUTION_REGISTRY: dict[str, RegistryEntry] = {}

def get_registry_key(year: int, day: int, part: int) -> str:
  return f"{year}:{day}:{part}"

def register(year: int, day: int, part: int, *, test: Optional[tuple[list[str], any]] = None):
  def outer(f):
    SOLUTION_REGISTRY[get_registry_key(year, day, part)] = RegistryEntry(year, day, part, f, test)
    
    @wraps(f)
    def inner(data: list[str]):
      return f(data)
    
    return inner
  
  return outer

def import_all_solutions(root: str):
    """Automatically import all solutions in the project"""
    root_path = Path(root)
    days = sorted(root_path.rglob("*/*.py"))

    for day in days:
        import_module(f"adventofcode.{day.parent.name}.{os.path.splitext(day.name)[0]}")