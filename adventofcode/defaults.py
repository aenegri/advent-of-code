import datetime
import os

from dotenv import load_dotenv


load_dotenv()


def get_latest_year() -> int:
  today = datetime.date.today()
  if today.month == 12:
    return today.year
  return today.year - 1


def get_latest_day():
  today = datetime.date.today()
  if today.month == 12 and today.day < 26:
    return today.day
  return 25


YEARS = range(2014, get_latest_year()+1)

DAYS = range(0, 26)

AOC_SESSION = os.environ.get("AOC_SESSION")
