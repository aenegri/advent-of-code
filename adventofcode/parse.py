import re

def ints(input: str) -> list[int]:
  return [int(match) for match in re.findall(r"-?\d+", input)]

def digits(input: str) -> list[int]:
  return [int(match) for match in re.findall(r"\d", input)]