

from collections import Counter
from adventofcode.parse import ints
from adventofcode.registry import register


test_data = """
3   4
4   3
2   5
1   3
3   9
3   3
"""

def parse_lists(lines: list[str]) -> tuple[list[int], list[int]]:
    a = []
    b = []

    for line in lines:
        nums = ints(line)
        a.append(nums[0])
        b.append(nums[1])

    return (a, b)


@register(2024, 1, 1, test=(test_data, 11))
def part_one(lines: list[str]):
    a, b = parse_lists(lines)

    total = 0
    for x, y in zip(sorted(a), sorted(b)):
        total += abs(x - y)
    
    return total

@register(2024, 1, 2, test=(test_data, 31))
def part_two(lines: list[str]):
    a, b = parse_lists(lines)

    freq = Counter(b)

    return sum(num * freq[num] for num in a)