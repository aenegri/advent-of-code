
from itertools import pairwise
from adventofcode.parse import ints
from adventofcode.registry import register

test_data = """
7 6 4 2 1
1 2 7 8 9
9 7 6 2 1
1 3 2 4 5
8 6 4 4 1
1 3 6 7 9
"""

def safe(report: list[int], tolerance: int = 0) -> bool:
  asc = report[0] < report[1]

  for idx in range(len(report)):
    if idx == len(report) - 1:
      continue

    l, r = report[idx], report[idx+1]

    diff = abs(l - r)
    out_of_bounds = diff < 1 or diff > 3
    wrong_dir = (asc and l > r) or (not asc and r > l)

    if (out_of_bounds or wrong_dir) and tolerance == 0:
      return False
    elif (out_of_bounds or wrong_dir) and tolerance > 0:
      # initial assumption on asc or dsc could be wrong
      if idx == 1 and wrong_dir:
        tmp = report.pop(idx - 1)
        if safe(report, tolerance=tolerance-1):
          report.insert(idx-1, tmp)
          return True
        report.insert(idx-1, tmp)

      tmp = report.pop(idx)
      if safe(report, tolerance=tolerance-1):
        report.insert(idx, tmp)
        return True
      report.insert(idx, tmp)

      tmp = report.pop(idx+1)
      if safe(report, tolerance=tolerance-1):
        report.insert(idx+1, tmp)
        return True
      report.insert(idx+1, tmp)
      
      return False
    
  return True

@register(2024, 2, 1, test=(test_data, 2))
def part_one(lines: list[str]):
  reports = [ints(line) for line in lines]
  return sum(1 for report in reports if safe(report))

@register(2024, 2, 2, test=(test_data, 4))
def part_two(lines: list[str]):
  reports = [ints(line) for line in lines]
  return sum(1 for report in reports if safe(report, tolerance=1))
