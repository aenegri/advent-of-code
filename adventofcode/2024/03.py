import re

from adventofcode.registry import register

test_data = """
xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))
"""

@register(2024, 3, 1, test=(test_data, 161))
def part_one(lines: list[str]):
  mul_pattern = r"mul\((\d{1,3}),(\d{1,3})\)"

  total = 0
  for line in lines:
    matches = re.findall(mul_pattern, line)

    for match in matches:
      total += int(match[0]) * int(match[1])
  
  return total

test_data_two = """
xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))
"""

@register(2024, 3, 2, test=(test_data_two, 48))
def part_two(lines: list[str]):
  pattern = r"(mul)\((\d{1,3}),(\d{1,3})\)|(do)\(\)|(don't)\(\)"
  
  total = 0
  active = True
  for line in lines:
    matches = re.findall(pattern, line)

    for match in matches:
      if match[0] == "mul" and active:
        total += int(match[1]) * int(match[2])
      elif match[3] == "do":
        active = True
      elif match[4] == "don't":
        active = False

  return total
