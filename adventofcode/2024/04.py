
from adventofcode.graphs import ALL, DIAGONALS, DOWN_LEFT, DOWN_RIGHT, UP_LEFT, UP_RIGHT, Matrix, Point
from adventofcode.registry import register

test_puzzle = """
MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX
"""

def word_search(puzzle: Matrix[str], target: str) -> int:
  count = 0
  for y in range(puzzle.height):
    for x in range(puzzle.width):
      pt = Point(x, y)
      if puzzle[pt] != target[0]:
        continue
      
      for dir in ALL:
        idx = 1
        next_pt = pt + dir
        found = True
        while idx < len(target):
          if next_pt not in puzzle or puzzle[next_pt] != target[idx]:
            found = False
            break
          idx += 1
          next_pt = next_pt + dir
        if found:
          count += 1
  return count

def x_mas_search(puzzle: Matrix[str]) -> int:
  count = 0
  for y in range(1, puzzle.height - 1):
    for x in range(1, puzzle.width - 1):
      pt = Point(x, y)
      if puzzle[pt] != "A":
        continue

      target = {"M", "S"}
      
      if not all(puzzle[pt+dir] in target for dir in DIAGONALS):
        continue

      if puzzle[pt+UP_LEFT] == puzzle[pt+DOWN_RIGHT] or puzzle[pt+DOWN_LEFT] == puzzle[pt+UP_RIGHT]:
        continue
      
      count += 1
  return count


@register(2024, 4, 1, test=(test_puzzle, 18))
def part_one(lines: list[str]):
  puzzle = Matrix.from_lines(lines)

  result = word_search(puzzle, "XMAS")
  return result

@register(2024, 4, 2, test=(test_puzzle, 9))
def part_two(lines: list[str]):
  puzzle = Matrix.from_lines(lines)
  return x_mas_search(puzzle)
