
import collections
from adventofcode.parse import ints
from adventofcode.registry import register

test_data = """
47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47
"""

def in_order(sequence: list[int], rules: dict[int, set[int]]) -> int:
  seen = set()
  for num in sequence:
    if rules[num].intersection(seen):
      return 0
    seen.add(num)
  return sequence[len(sequence) // 2]

def fix_sequence(sequence: list[int], rules: dict[int, set[int]]) -> None:
  seen = set()
  for i in range(len(sequence)):
    num = sequence[i]
    conflicts = rules[num].intersection(seen)
    seen.add(num)
    if not conflicts:
      continue

    j = i
    while conflicts:
      j -= 1 
      if sequence[j] not in conflicts:
        continue
      conflicts.remove(sequence[j])

    temp = sequence.pop(i)
    sequence.insert(j, temp)

@register(2024, 5, 1, test=(test_data, 143))
def part_one(lines: list[str]):
  sections = "\n".join(lines).split("\n\n")

  rules: dict[int, set[int]] = collections.defaultdict(set)
  for line in sections[0].split("\n"):
    vals = ints(line)
    before, after = vals[0], vals[1]
    rules[before].add(after)

  return sum(in_order(ints(line), rules) for line in sections[1].split("\n"))



@register(2024, 5, 2, test=(test_data, 123))
def part_two(lines: list[str]):
  sections = "\n".join(lines).split("\n\n")

  rules: dict[int, set[int]] = collections.defaultdict(set)
  for line in sections[0].split("\n"):
    vals = ints(line)
    before, after = vals[0], vals[1]
    rules[before].add(after)

  invalid_sequences: list[list[int]]= []
  for line in sections[1].split("\n"):
    sequence = ints(line)
    if in_order(sequence, rules) == 0:
      invalid_sequences.append(sequence)

  for seq in invalid_sequences:
    fix_sequence(seq, rules)

  return sum(seq[len(seq) // 2] for seq in invalid_sequences)
