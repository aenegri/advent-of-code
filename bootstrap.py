from argparse import ArgumentParser
import os

import requests

from adventofcode.defaults import AOC_SESSION, DAYS, YEARS, get_latest_day, get_latest_year

template = """
from adventofcode.registry import register

@register({year}, {day}, 1)
def part_one(lines: list[str]):
  pass

@register({year}, {day}, 2)
def part_two(lines: list[str]):
  pass
"""

def create_solution_file(year: int, day: int):
  root = os.path.dirname(os.path.abspath(__file__))
  year_module_dir = os.path.join(root, "adventofcode", str(year))
  target_path = os.path.join(year_module_dir, f"{day:02}.py")
  
  if not os.path.exists(year_module_dir):
    os.mkdir(year_module_dir)
    
  if os.path.exists(target_path):
    print(f"❗Skipping solution file generation -- file already on disk")
    return
    
  f = open(target_path, "w+")
  data = template.format(year=year, day=day)
  f.write(data)
  f.close()
  
  print(f"✅Generated solution file at {target_path}")
  
def download_input(session: str, year: int, day: int):
  root = os.path.dirname(os.path.abspath(__file__))
  year_inputs_dir = os.path.join(root, "inputs", str(year))
  target_path = os.path.join(year_inputs_dir, f"{day:02}.txt")
  
  if not os.path.exists(year_inputs_dir):
    os.mkdir(year_inputs_dir)
    
  if os.path.exists(target_path):
    print("❗Skipping input file download -- already on disk")
    return
  
  input_url = f"https://adventofcode.com/{year}/day/{day}/input"
  
  r = requests.get(input_url, cookies={"session": session})
  
  if r.status_code != 200:
    raise Exception(f"Failed to retrieve inputs from {input_url}. Status code: {r.status_code}")
  
  f = open(target_path, "w+")
  f.write(r.text)
  f.close()
  print(f"✅Downloaded input file at {target_path}")

  
parser = ArgumentParser()

parser.add_argument(
  "--year",
  choices=YEARS,
  default=get_latest_year(),
  type=int,
)

parser.add_argument(
  "--day",
  choices=DAYS,
  default=get_latest_day(),
  type=int,
)

args = parser.parse_args()

create_solution_file(args.year, args.day)

if AOC_SESSION is not None:
  download_input(AOC_SESSION, args.year, args.day)
else:
  print("❗Skipping input file download -- AOC_SESSION env variable is missing")
  
print(f"🔗Visit https://adventofcode.com/{args.year}/day/{args.day} to read the problem and submit")