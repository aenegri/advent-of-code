import argparse
from enum import Enum
import os
import time
from typing import Optional

from prettytable import PrettyTable
import requests
from colorist import Color
from adventofcode.defaults import AOC_SESSION, DAYS, YEARS, get_latest_day, get_latest_year

from adventofcode.registry import SOLUTION_REGISTRY, get_registry_key, import_all_solutions

def get_input_for_day(year: int, day: int, *, root: str, input_override: Optional[str] = None) -> list[str]:
    """
    Get the input for the year/day as list of strings
    """
    if input_override is not None:
      input_file = input_override
    else:
      input_file = os.path.join(root, "inputs", str(year), f"{day:02}.txt")
      
    data = open(input_file).read().strip()
    return [line.strip() for line in data.split("\n")]
  
def format_elapsed_time(elapsed: float) -> str:
  if elapsed < 1.0:
    elapsed_ms = elapsed * 1000
    return f"{int(elapsed_ms)}ms"
  elif elapsed < 60.0:
    return f"{elapsed:.3f}s"
  else:
    elapsed_mins = elapsed // 60
    elapsed_secs = int(elapsed) % 60
    return f"{elapsed_mins}m {elapsed_secs}s"
  
class SubmissionResult(Enum):
  SUCCESS = "SUCCESS"
  TOO_HIGH = "TOO_HIGH"
  TOO_LOW = "TOO_LOW"
  TOO_RECENT = "TOO_RECENT"
  WRONG = "WRONG"
  ALREADY_COMPLETE = "ALREADY_COMPLETE"
  
def submit_answer(answer: str, year: int, day: int, part: int, *, session: str):
  submission = {
    "level": part,
    "answer": answer,
  }
  submit_url = f"https://adventofcode.com/{year}/day/{day}/answer"
  response = requests.post(
    submit_url,
    data=submission,
    cookies={"session": session},
  )
  
  if response.status_code != 200:
    raise Exception(f"Failed to submit answer to {submit_url}. Status code: {response.status_code}")
  
  if "That's the right answer" in response.text:
    return SubmissionResult.SUCCESS
  elif "your answer is too low" in response.text:
    return SubmissionResult.TOO_LOW
  elif "your answer is too high" in response.text:
    return SubmissionResult.TOO_HIGH
  elif "That's not the right answer" in response.text:
    return SubmissionResult.WRONG
  elif "You gave an answer too recently" in response.text:
    return SubmissionResult.TOO_RECENT
  elif "Did you already complete it" in response.text:
    return SubmissionResult.ALREADY_COMPLETE
  else:
    raise Exception("Failed to parse submission response")


def run_solution(year: int, day: int, part: int, *, test: bool = False, submit: bool = False, session: Optional[str] = None):
  if test and submit:
    raise Exception("Cannot test and submit at same time")
  
  root = os.path.dirname(os.path.abspath(__file__))
  solution_root = os.path.join(root, "adventofcode")
  
  import_all_solutions(solution_root)
  entry = SOLUTION_REGISTRY.get(get_registry_key(year, day, part))
  
  assert entry is not None, f"Solution not registered for {year=} {day=} {part=}"
  solution_fn = entry.solution
  
  if test:
    assert entry.test is not None, "Test must be registered"
    test_data = entry.test[0]
    if isinstance(test_data, list):
      lines = test_data
    elif isinstance(test_data, str):
      lines = [line.strip() for line in test_data.strip().split("\n")]
  else:
    lines = get_input_for_day(year, day, root=root)
  
  start = time.perf_counter()
  solution = solution_fn(lines)
  elapsed = (time.perf_counter() - start)
  
  submit_result = None
  if submit:
    submit_result = submit_answer(str(solution), year, day, part, session=session)

  
  table = PrettyTable(
    title=f"AoC {year} Day {day} Part {part}",
    encoding="utf-8",
    align="l",
    header=False,
  )    
  
  if test and entry.test[1] == solution:
    table.add_row(("✅Test Passed",))
  elif test:
    table.add_row((f"❌Test Failed, expected: {entry.test[1]}, got: {solution}",))
  else:
    table.add_row((f"🎉Solution: {solution}",))
    
  table.add_row((f"⏰Time: {format_elapsed_time(elapsed)}",))
  
  if submit_result is not None:
    if submit_result == SubmissionResult.SUCCESS:
      msg_color = Color.GREEN
    elif submit_result == SubmissionResult.ALREADY_COMPLETE:
      msg_color = Color.YELLOW
    else:
      msg_color = Color.RED
    submit_result_msg = f"{msg_color}{submit_result.value}{Color.OFF}"
  else:
    submit_result_msg = "n/a"
    
  table.add_row((f"⭐Submission Result: {submit_result_msg}",))
  
  print(table)
  

parser = argparse.ArgumentParser(
  prog="aoc",
  description="Executes solutions for advent of code challenges",
)

parser.add_argument(
  "--year",
  choices=YEARS,
  default=get_latest_year(),
  type=int,
)

parser.add_argument(
  "--day",
  choices=DAYS,
  default=get_latest_day(),
  type=int,
)

parser.add_argument(
  "--part",
  choices=[1,2],
  type=int,
)

parser.add_argument(
  "--test",
  choices=[True, False],
  type=bool,
  default=False,
  help="runs test case if configured"
)

parser.add_argument(
  "--submit",
  choices=[True, False],
  type=bool,
  default=False,
  help="submits answer online"
)

args = parser.parse_args()

if args.submit:
  assert AOC_SESSION is not None, "AOC_SESSION required to submit"

run_solution(args.year, args.day, args.part, test=args.test, submit=args.submit, session=AOC_SESSION)
  